################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../L5_Application/source/Assign2.cpp \
../L5_Application/source/Assign3.cpp \
../L5_Application/source/Assign4.cpp \
../L5_Application/source/Assign5.cpp \
../L5_Application/source/high_level_init.cpp \
../L5_Application/source/remote.cpp \
../L5_Application/source/terminal.cpp 

OBJS += \
./L5_Application/source/Assign2.o \
./L5_Application/source/Assign3.o \
./L5_Application/source/Assign4.o \
./L5_Application/source/Assign5.o \
./L5_Application/source/high_level_init.o \
./L5_Application/source/remote.o \
./L5_Application/source/terminal.o 

CPP_DEPS += \
./L5_Application/source/Assign2.d \
./L5_Application/source/Assign3.d \
./L5_Application/source/Assign4.d \
./L5_Application/source/Assign5.d \
./L5_Application/source/high_level_init.d \
./L5_Application/source/remote.d \
./L5_Application/source/terminal.d 


# Each subdirectory must supply rules for building sources it contributes
L5_Application/source/%.o: ../L5_Application/source/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: Cross ARM C++ Compiler'
	arm-none-eabi-g++ -mcpu=cortex-m3 -mthumb -Os -fmessage-length=0 -ffunction-sections -fdata-sections -Wall -Wshadow -Wlogical-op -Wfloat-equal -DBUILD_CFG_MPU=0 -I"C:\SJSU_Dev\projects\lpc1758_freertos_RC" -I"C:\SJSU_Dev\projects\lpc1758_freertos_RC\newlib" -I"C:\SJSU_Dev\projects\lpc1758_freertos_RC\L0_LowLevel" -I"C:\SJSU_Dev\projects\lpc1758_freertos_RC\L1_FreeRTOS" -I"C:\SJSU_Dev\projects\lpc1758_freertos_RC\L1_FreeRTOS\include" -I"C:\SJSU_Dev\projects\lpc1758_freertos_RC\L1_FreeRTOS\portable" -I"C:\SJSU_Dev\projects\lpc1758_freertos_RC\L2_Drivers" -I"C:\SJSU_Dev\projects\lpc1758_freertos_RC\L2_Drivers\base" -I"C:\SJSU_Dev\projects\lpc1758_freertos_RC\L3_Utils" -I"C:\SJSU_Dev\projects\lpc1758_freertos_RC\L3_Utils\tlm" -I"C:\SJSU_Dev\projects\lpc1758_freertos_RC\L4_IO" -I"C:\SJSU_Dev\projects\lpc1758_freertos_RC\L4_IO\fat" -I"C:\SJSU_Dev\projects\lpc1758_freertos_RC\L4_IO\wireless" -I"C:\SJSU_Dev\projects\lpc1758_freertos_RC\L5_Application" -std=gnu++11 -fabi-version=0 -fno-exceptions -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -c -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


