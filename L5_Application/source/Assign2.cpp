/*program is used for testing GPIO ports
 * using interrupt  eint3_enable_port0 to set flag (used to avoid polling)
 * eint3 is initialized in peroid_callbacks.cpp init section
 * led_task() is called in periodic scheduler in 10hz cycle
 * when it receives high on port 2.0
 * led on board is lights up
 */

#include "Assign2.hpp"
#include "gpio.hpp"
#include "io.hpp"
#include "tasks.hpp"
#include "lpc_sys.h"

static bool ledSTATUS;
static int initialTime;
static int currentTime;

void setFlag(void)
{
	ledSTATUS = 1;
}

void setOutput(void)
{
	static GPIO pinSignalOut(P2_0);
	pinSignalOut.setAsOutput();
	if(ledSTATUS == 1)
	{
		LE.on(1);
		pinSignalOut.setHigh();
		initialTime = sys_get_uptime_ms();
		ledSTATUS=0;
	}

	currentTime = sys_get_uptime_ms();

	if((currentTime - initialTime) >= 500)
	{
		LE.off(1);
		pinSignalOut.setLow();
	}
}

void led_task(void)
{

static GPIO port2_0(P2_0);;

port2_0.setAsInput();
if (port2_0.read())
   {
	 // Turn on LED4
	 LE.on(4);
    }
	else
	{
	 // Turn off LED4
	 LE.off(4);
	}
}
