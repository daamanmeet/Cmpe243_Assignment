//CAN communication assignment

#include "Assign4.hpp"
#include <stdio.h>
#include "gpio.hpp"
#include "can.h"
#include "tasks.hpp"
#include "io.hpp"
#include <stdint.h>

can_msg_t message;

void canbus_check()
{
	if(CAN_is_bus_off(can1))
	{
		CAN_reset_bus(can1);
	}

}

void can1_init(void)
{
	CAN_init(can1,100,10,10,NULL,NULL);
	CAN_reset_bus(can1);
	CAN_bypass_filter_accept_all_msgs();
/*
	const can_std_id_t slist[]      = { CAN_gen_sid(can1, 0x100), CAN_gen_sid(can1, 0x110),   // 2 entries
	                                     CAN_gen_sid(can1, 0x120), CAN_gen_sid(can1, 0x130)    // 2 entries
	};
	CAN_setup_filter(slist, 4 , NULL, 0, NULL, 0, NULL, 0);
*/
	message.msg_id = 0x122;
	message.frame_fields.is_29bit = 0;
	message.frame_fields.data_len = 8;
	CAN_reset_bus(can1);
}

void txd_task(void)
{

	//static GPIO port2_0(P2_0);

	//port2_0.setAsInput();
	//if (port2_0.read())
	if (SW.getSwitch(1))
	{
		message.data.bytes[0] = 4;
		if(false == CAN_tx(can1, &message, portMAX_DELAY))
			{
		        LD.setNumber(11);
			}
	    }
		else
		{
			message.data.bytes[0] = 2;
			if(false == CAN_tx(can1, &message, portMAX_DELAY))
			{
				 LD.setNumber(00);

			}
		}
}


void rxd_task(void)
{
	can_msg_t msg1;

		msg1.frame = 0x0;
		msg1.msg_id = 0x0;
		msg1.frame_fields.is_29bit = 0;
		msg1.frame_fields.data_len = 1;
		msg1.data.qword = 0x00;

		CAN_rx(can1, &msg1, 0);

		LE.setAll(msg1.data.qword);

}


