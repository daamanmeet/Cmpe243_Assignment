/*UART Communication Assignment
 * Sending light_senser_value through uart2 to uart3
 *displaying value on ledSegment
 */

#include "Assign3.hpp"
#include "io.hpp"
#include "uart2.hpp"
#include "uart3.hpp"

static Uart2& u2 = Uart2::getInstance();
static Uart3& u3 = Uart3::getInstance();

void uart_init(void)
{
	u2.init(9600,100,100);
	u3.init(9600,100,100);
}

void uart_tx(void)
{
	//light sensorvalue in percentage is 8 bit so char is fine
	char x = LS.getPercentValue();
	u2.putChar(x,200);


	/*if using bigger values like int have to use buffers
	 *
	 *
	//int lvs= TS.getFarenheit();
	int lvs= LS.getRawValue();
	char tx[4];
	tx[0] = lvs;
	tx[1] = lvs>>8;
	tx[2] = lvs>>16;
	tx[3] = lvs>>24;
	printf("tx: %i\n",lvs);
	u2.putChar(tx[0],200);
	u2.putChar(tx[1],200);
	u2.putChar(tx[2],200);
	u2.putChar(tx[3],200);
	printf("tx s : %d\n",lvs);
	*/
}

void uart_rx(void)
{
	char xa;
	u3.getChar(&xa,200);
    LD.setNumber(xa);

	/*code for uart receiver
	 * getting int on  uart3
	int rxv;
	char buffer[4];
	u3.gets(buffer,4,200);
	rxv = buffer[0]|buffer[1]<<8|buffer[2]<<16|buffer[3]<<24;
	LD.setNumber(rxv);
    printf("rx : %d\n",rxv);
    */
}
