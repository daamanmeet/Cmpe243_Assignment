//Assignment5 - Periodic Scheduler

#include "Assign5.hpp"
#include <stdio.h>
#include "gpio.hpp"
#include "file_logger.h"
#include "tasks.hpp"
#include "io.hpp"
#include <stdint.h>

static int filtered_reading = 0;

void task_10hz(void)
{
	static int count = 0,readings = 0;
	int light_value = LS.getRawValue();

	readings = readings + light_value;
	count++;

	if(count == 3)
	{
		filtered_reading = readings/count;
		count = 0;
		readings = 0;
	}
}

void task_1hz(void)
{
	//printf("filtered sensor value: %i\n",filtered_reading);
	LOG_INFO("Filtered Sensor value: %i\n",filtered_reading);
}
